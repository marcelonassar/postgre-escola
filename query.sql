﻿CREATE TABLE assiste (
 id serial PRIMARY KEY,
 aula_id INTEGER REFERENCES Aula(id),
 aluno_id INTEGER REFERENCES Aluno(id)
) 